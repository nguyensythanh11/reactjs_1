import logo from './logo.svg';
import './App.css';
import Header from './BTLayout/Header/Header';
import Footer from './BTLayout/Footer/Footer';
import Body from './BTLayout/Body/Body';

function App() {
  return (
    <div className="App ">
      <Header></Header>
      <Body></Body>
      <Footer></Footer>
    </div>
  );
}

export default App;
