import React from 'react'
import './style.scss'

const Header = () => {
  return (
    <header className='Header bg-dark'>
        <div className="container d-flex justify-content-between align-items-center">
            <h3>Start Bootstrap</h3>
            <nav>
                <a href="" className='active'>Home</a>
                <a href="">About</a>
                <a href="">Services</a>
                <a href="">Contact</a>
            </nav>
        </div>
    </header>
  )
}

export default Header