import React from 'react'
import Banner from './Banner'
import Item from './Item'
import './style.scss'

const Body = () => {
  return (
    <section className='Body my-5 container'>
        <Banner></Banner>
        <div className='row mt-5'>
            <div className="col-2">
                <Item></Item>
            </div>
            <div className="col-2">
                <Item></Item>
            </div>
            <div className="col-2">
                <Item></Item>
            </div>
            <div className="col-2">
                <Item></Item>
            </div>
        </div>
    </section>
  )
}

export default Body