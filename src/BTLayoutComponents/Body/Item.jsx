import React from 'react'
import './style.scss'

const Item = () => {
  return (
    <div className='Item'>
        <div class="card" style={{width: "260px"}}>
            <div className='card-header d-flex justify-content-center align-items-center' style={{height: "15rem"}}>500x235</div>
            <div class="card-body">
                <h5 class="card-title">Card title</h5>
                <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
            </div>
            <div className='card-footer'>
                <a href="#" class="btn btn-primary">Find out more!</a>
            </div>
        </div>
    </div>
  )
}

export default Item