import React from 'react'
import './style.scss'

const Banner = () => {
  return (
    <div className='Banner'>
        <h2>A warm welcome!</h2>
        <p>Bootstrap utility classes are used to create this jumbotron since the old component has been removed from the framework. Why create custom CSS when you can use utilities?</p>
        <button className='btn btn-primary'>Call to action</button>
    </div>
  )
}

export default Banner