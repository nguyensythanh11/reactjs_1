import React from 'react'
import './style.scss'

const Footer = () => {
  return (
    <footer className='Footer bg-dark'>
        <p>Copyright &copy Your Website 2019</p>
    </footer>
  )
}

export default Footer